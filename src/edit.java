
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Supawee
 */
public class edit {

    static Scanner kb = new Scanner(System.in);
    static String name, surname, username, password, tel, weight, height;

    public static void showEdit() {
        System.out.println("1.Edit your detail");
        System.out.println("2.Back");
        System.out.print("Please input number (1,2) : ");
        checkInput(kb.next());
    }

    public static void checkInput(String input) {
        while (true) {
            try {
                int choose = Integer.parseInt(input);
                if (choose == 1) { //edit
                    loadData();
                } else if (choose == 2) { //menu
                    menu.showMenu();
                } else { //error
                    printError();
                    showEdit();
                }
                break;
            } catch (Exception e) {
                printError();
                showEdit();
            }
        }
    }

    public static void loadData() {
        while (true) {
            inputData();
            if (!checkTel(tel)) {
                printErrorTel();
            } else if (!checkWeightHeight(weight, height)) {
                printErrorWeightHeight();
            } else {
                user.editUser(name, surname, login.getUsername(), password, tel, Double.parseDouble(weight), Double.parseDouble(height));
                printSuccess();
                menu.showMenu();
                break;
            }
            askBack();
        }
    }

    public static void askBack() {
        System.out.println("Do you want to back to menu ?");
        System.out.println("0.Yes");
        System.out.println("1.No");
        checkInputAskBack(kb.next());

    }

    public static void checkInputAskBack(String input) {
        while (true) {
            try {
                int choose = Integer.parseInt(input);
                if (choose == 0) { //login
                    menu.showMenu();
                } else if (choose == 1) { //exit
                    showEdit();
                } else { //error
                    printErrorInputAskBack();
                    askBack();
                }
                break;
            } catch (Exception e) {
                printErrorInputAskBack();
                askBack();
            }
        }
    }

    public static void printErrorInputAskBack() {
        System.out.println("Error : Please input number 0 or 1");
    }

    public static boolean checkTel(String tel) {
        try {
            Integer.parseInt(tel);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean checkWeightHeight(String weight, String height) {
        try {
            Double.parseDouble(weight);
            Double.parseDouble(height);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    private static void printErrorTel() {
        System.out.println("Error : tel must be number");
    }

    private static void printErrorWeightHeight() {
        System.out.println("Error : weight and height must be number");
    }

    private static void printSuccess() {
        System.out.println("Edit successful");
    }

    public static void inputData() {
        System.out.print("Name : ");
        name = kb.next();
        System.out.print("Surname : ");
        surname = kb.next();
        System.out.print("Password : ");
        password = kb.next();
        System.out.print("Tel : ");
        tel = kb.next();
        System.out.print("Weight : ");
        weight = kb.next();
        System.out.print("Height : ");
        height = kb.next();
    }

    public static void printError() {
        System.out.println("Error : Please input (1-2)");
    }
}
