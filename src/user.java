
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author informatics
 */
public class user {

    static ArrayList<user> user_detail = new ArrayList<>();
    private String name;
    private String surname;
    private String username;
    private String password;
    private String tel;
    private double weight;
    private double height;

    public static ArrayList<user> getUser_detail() {
        return user_detail;
    }

    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public String getTel() {
        return tel;
    }

    public double getWeight() {
        return weight;
    }

    public double getHeight() {
        return height;
    }

    public user(String name, String surname, String username, String password, String tel, double weight, double height) {
        this.name = name;
        this.surname = surname;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.weight = weight;
        this.height = height;
    }

    String getUsername() {
        return this.username;
    }

    String getPassword() {
        return this.password;
    }

    public static void addUser(String newname, String newsurname, String newusername, String newpassword, String newtel, double newweight, double newheight) {
        user_detail.add(new user(newname, newsurname, newusername, newpassword, newtel, newweight, newheight));
    }

    public static void editUser(String newname, String newsurname, String username, String newpassword, String newtel, double newweight, double newheight) {
        for (user a : user_detail) {
            if (username.equals(a.getUsername())) {
                System.out.println(a.getPassword());
                a.name = newname;
                a.surname = newsurname;
                a.password = newpassword;
                a.tel = newtel;
                a.weight = newweight;
                a.height = newheight;
                System.out.println(a.getPassword());
            }
        }
    }

    public static boolean checkID(String username, String password) {
        if (user_detail.size() == 0) {
            user_detail.add(new user("admin", "admin", "admin", "admin", "0993322733", 185.5, 70));
        }
        for (user a : user_detail) {
            if (username.equals(a.getUsername()) && password.equals(a.getPassword())) {
                return true;
            }
        }
        return false;
    }

    public static boolean checkUsername(String username) {
        for (user a : user_detail) {
            if (username.equals(a.getUsername())) {
                return true;
            }
        }
        return false;
    }
    
    public String toString(){
        return this.name+" "+this.surname+" "+this.tel+" "+this.weight+" "+this.height;
    }

    /*public static boolean checkName(String name, String surname) {
     for (user a : user_detail) {
     if (name.equals(a.getName()) && surname.equals(a.getSurname())) {
     return true;
     }
     }
     return false;
     }*/
}
