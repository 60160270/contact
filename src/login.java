
import java.util.*;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author informatics
 */
public class login {

    static Scanner kb = new Scanner(System.in);
    static String username, password;

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        login();
    }

    public static void login() {
        showLogin();

    }

    public static void showLogin() {
        System.out.println("1.Login");
        System.out.println("2.Exit");
        System.out.print("Please input number (1-2): ");
        checkInput(kb.next());
    }

    public static void checkInput(String input) {
        while (true) {
            try {
                int choose = Integer.parseInt(input);
                if (choose == 1) { //login
                    inputID();
                } else if (choose == 2) { //exit
                    System.out.println("Program is exit");
                } else { //error
                    printErrorInput();
                    showLogin();
                }
                break;
            } catch (Exception e) {
                printErrorInput();
                showLogin();
            }
        }
    }

    public static void printErrorInput() {
        System.out.println("Error : Please input number 1 or 2");
    }

    public static void inputID() {
        System.out.print("Username : ");
        username = kb.next();
        System.out.print("Password : ");
        password = kb.next();
        if (user.checkID(username, password)) {
            menu.showMenu();
            //System.out.println("Login success");
        } else {
            printErrorID();
            askBack();
        }
    }

    public static void printErrorID() {
        System.out.println("Error : Username or Password incorrect");
    }

    public static void askBack() {
        System.out.println("Do you want to exit program ?");
        System.out.println("0.Yes");
        System.out.println("1.No");
        checkInputAskBack(kb.next());

    }

    public static void checkInputAskBack(String input) {
        while (true) {
            try {
                int choose = Integer.parseInt(input);
                if (choose == 0) { //login
                    System.out.println("Program is exit");
                } else if (choose == 1) { //exit
                    inputID();
                } else { //error
                    printErrorInputAskBack();
                    askBack();
                }
                break;
            } catch (Exception e) {
                printErrorInputAskBack();
                askBack();
            }
        }
    }

    public static void printErrorInputAskBack() {
        System.out.println("Error : Please input number 0 or 1");
    }
    
    public static String getUsername(){
        return username;
    }

}
