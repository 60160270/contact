
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Supawee
 */
public class menu {

    static Scanner kb = new Scanner(System.in);

    public static void showMenu() {
        System.out.println("1.Add User");
        System.out.println("2.Edit User");
        System.out.println("3.Show User");
        System.out.println("4.Logout");
        System.out.print("Please input number (1-4): ");
        checkInput(kb.next());
    }

    public static void checkInput(String input) {
        while (true) {
            try {
                int choose = Integer.parseInt(input);
                if (choose == 1) { //add
                    add.showAdd();
                } else if (choose == 2) { //edit
                    edit.showEdit();
                } else if (choose == 3) { //show
                    detail.showUser();
                } else if (choose == 4) { //logout
                    login.showLogin();
                } else { //error
                    printError();
                    showMenu();
                }
                break;
            } catch (Exception e) {
                printError();
                showMenu();
            }
        }
    }

    public static void printError() {
        System.out.println("Error : Number must be 1 - 4");
    }
}
