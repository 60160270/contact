
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Supawee
 */
public class detail {

    static Scanner kb = new Scanner(System.in);

    public static void showUser() {
        System.out.println("1.Search by username");
        System.out.println("2.Show all user");
        System.out.println("3.Back");
        System.out.print("Please input number ( 1 - 3 ) : ");
        checkInput(kb.next());
    }

    public static void checkInput(String input) {
        while (true) {
            try {
                int choose = Integer.parseInt(input);
                if (choose == 1) { //search by id
                    searchUser();
                } else if (choose == 2) { //all
                    showAllUser();
                } else if (choose == 3) { //back
                    menu.showMenu();
                } else { //error
                    printError();
                    showUser();
                }
                break;
            } catch (Exception e) {
                printError();
                showUser();
            }
        }
    }

    public static void printError() {
        System.out.println("Error : Please input number ( 1 - 3 )");
    }

    public static void showAllUser() {
        for (user a : user.user_detail) {
            System.out.println(a.toString());
        }
        askBack();
    }

    private static void searchUser() {
        System.out.print("Please input username : ");
        String username = kb.next();
        for (user a : user.user_detail) {
            if (a.getUsername().equals(username)) {
                System.out.println(a.toString());
                askBack();
            }
        }
        printNoUser();
        askBack();
    }

    
    public static void printNoUser() {
        System.out.println("Dont have this user");
    }

    public static void askBack() {
        System.out.println("Do you want to back to menu ?");
        System.out.println("0.Yes");
        System.out.println("1.No");
        checkInputAskBack(kb.next());

    }

    public static void checkInputAskBack(String input) {
        while (true) {
            try {
                int choose = Integer.parseInt(input);
                if (choose == 0) { //login
                    menu.showMenu();
                } else if (choose == 1) { //exit
                    showUser();
                } else { //error
                    printErrorInputAskBack();
                    askBack();
                }
                break;
            } catch (Exception e) {
                printErrorInputAskBack();
                askBack();
            }
        }
    }

    public static void printErrorInputAskBack() {
        System.out.println("Error : Please input number 0 or 1");
    }
}
