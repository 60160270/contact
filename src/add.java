
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author Supawee
 */
public class add {

    static Scanner kb = new Scanner(System.in);
    static String name, surname, username, password, tel, weight, height;

    public static void showAdd() {
        while (true) {
            inputData();
            if (user.checkUsername(username)) {
                printErrorUsername();
            } else if (!checkTel(tel)) {
                printErrorTel();
            } else if (!checkWeightHeight(weight, height)) {
                printErrorWeightHeight();
            } else {
                user.addUser(name, surname, username, password, tel, Double.parseDouble(weight), Double.parseDouble(height));
                printSuccess();
                menu.showMenu();
                break;
            }
            askBack();
        }
    }

    public static void inputData() {
        System.out.print("Name : ");
        name = kb.next();
        System.out.print("Surname : ");
        surname = kb.next();
        System.out.print("Username : ");
        username = kb.next();
        System.out.print("Password : ");
        password = kb.next();
        System.out.print("Tel : ");
        tel = kb.next();
        System.out.print("Weight : ");
        weight = kb.next();
        System.out.print("Height : ");
        height = kb.next();
    }

    public static void askBack() {
        System.out.println("Do you want to back to menu ?");
        System.out.println("0.Yes");
        System.out.println("1.No");
        checkInputAskBack(kb.next());

    }

    public static void checkInputAskBack(String input) {
        while (true) {
            try {
                int choose = Integer.parseInt(input);
                if (choose == 0) { //login
                    menu.showMenu();
                } else if (choose == 1) { //exit
                    showAdd();
                } else { //error
                    printErrorInputAskBack();
                    askBack();
                }
                break;
            } catch (Exception e) {
                printErrorInputAskBack();
                askBack();
            }
        }
    }

    public static void printErrorInputAskBack() {
        System.out.println("Error : Please input number 0 or 1");
    }

    public static boolean checkTel(String tel) {
        try {
            Integer.parseInt(tel);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean checkWeightHeight(String weight, String height) {
        try {
            Double.parseDouble(weight);
            Double.parseDouble(height);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static void printErrorUsername() {
        System.out.println("Error : username exist");
    }

    private static void printErrorTel() {
        System.out.println("Error : tel must be number");
    }

    private static void printErrorWeightHeight() {
        System.out.println("Error : weight and height must be number");
    }

    private static void printSuccess() {
        System.out.println("Add successful");
    }
}
